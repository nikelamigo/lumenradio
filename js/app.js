"use strict";

/* ^^^
 * JQUERY Actions
 * ========================================================================== */
$(function () {
  'use strict';

  $.exists = function (selector) {
    return $(selector).length > 0;
  };

  $(document).on('click', function (event) {
    if ($(event.target).closest('.app-header__search').length) {
      return;
    }

    $('.app-header__search').removeClass('show');
  });
  $(window).on('scroll', function () {
    var old_scroll = $(window).scrollTop();

    if (old_scroll >= 10) {
      $(".app-header__topline").addClass('scroller');
    } else {
      $(".app-header__topline").removeClass('scroller');
    }
  }).trigger('scroll');
  /**
   * [^_]*.js - выборка всех файлов, которые не начинаются с подчеркивания
   */

  $('.app-header__search-button').on('click', function (event) {
    event.preventDefault();
    $(this).closest('.app-header__search').toggleClass('show').find('.app-header__search-input > input[type=search]').focus();
  });
  $('.app-header__hamburger').on('click', function (event) {
    event.preventDefault();
    $('body').toggleClass('disable-scroll').find('.app-header__topline').toggleClass('showmenu');
  });
  $('.l-articles__items.slider').addClass('owl-carousel').owlCarousel({
    items: 3,
    autoplay: false,
    mouseDrag: false,
    touchDrag: true,
    nav: true,
    dots: false,
    navText: ['<svg xmlns="http://www.w3.org/2000/svg" width="14" height="26" viewBox="0 0 14 26" fill="none"><path d="M1.77036 25.3287C1.59671 25.5024 1.37966 25.5892 1.1409 25.5892C0.90214 25.5892 0.685085 25.5024 0.511442 25.3287C0.164155 24.9814 0.164155 24.4171 0.511442 24.0698L11.5812 13L0.511442 1.93027C0.164155 1.58298 0.164155 1.01864 0.511442 0.671354C0.858729 0.324067 1.42307 0.324067 1.77036 0.671354L13.4696 12.3706C13.8169 12.7179 13.8169 13.2822 13.4696 13.6295L1.77036 25.3287Z"/></svg>', '<svg xmlns="http://www.w3.org/2000/svg" width="14" height="26" viewBox="0 0 14 26" fill="none"><path d="M1.77036 25.3287C1.59671 25.5024 1.37966 25.5892 1.1409 25.5892C0.90214 25.5892 0.685085 25.5024 0.511442 25.3287C0.164155 24.9814 0.164155 24.4171 0.511442 24.0698L11.5812 13L0.511442 1.93027C0.164155 1.58298 0.164155 1.01864 0.511442 0.671354C0.858729 0.324067 1.42307 0.324067 1.77036 0.671354L13.4696 12.3706C13.8169 12.7179 13.8169 13.2822 13.4696 13.6295L1.77036 25.3287Z""/></svg>'],
    loop: true,
    margin: 30,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 2
      },
      900: {
        items: 3
      }
    }
  });
  ;

  (function () {
    $.preventScrolling = function (selector, options) {
      // запрещаем прокрутку страницы при прокрутке элемента
      var defaults = {
        classes: {
          scrolled: 'is-scrolled',
          onTop: 'is-onTop',
          onBottom: 'is-onBottom'
        },
        onTop: function onTop() {},
        onBottom: function onBottom() {}
      };
      var options = $.extend({}, defaults, options);
      var scroller = $(selector);
      scroller.on('scroll', function () {
        if (scroller.scrollTop() == 0) {
          scroller.addClass(options.classes.onTop).removeClass(options.classes.onBottom);
        }

        if (scroller.scrollTop() == scroller[0].scrollHeight - scroller.height()) {
          scroller.removeClass(options.classes.onTop).addClass(options.classes.onBottom);
        }
      });

      if (scroller[0].scrollHeight > scroller.height()) {
        scroller.addClass('with-scroll');
      } else {
        scroller.removeClass('with-scroll');
      }

      $(window).on('resize', function () {
        if (scroller[0].scrollHeight > scroller.height()) {
          scroller.addClass('with-scroll');
        } else {
          scroller.removeClass('with-scroll');
        }
      });
      scroller.off('mousewheel DOMMouseScroll').on('mousewheel DOMMouseScroll', function (e) {
        var scrollTo = null;

        if (e.type == 'mousewheel') {
          scrollTo = e.originalEvent.wheelDelta * -1;
        } else if (e.type == 'DOMMouseScroll') {
          scrollTo = 40 * e.originalEvent.detail;
        }

        if (scrollTo && scroller[0].scrollHeight > scroller.height()) {
          e.stopPropagation();
          e.preventDefault();
          $(this).scrollTop(scrollTo + $(this).scrollTop());
        }
      });
    };
  })();
  /**
   * Мобильное меню сайта
   */


  var asideMenuBtn = $('.l-aside-menu-btn');
  var asideMenu = $('.l-aside-menu');
  var asideHead = $('.l-aside-menu__head');
  var asideMenuContent = $('.l-aside-menu__content');
  var asideMenuScroller = $('.l-aside-menu__scroller-content');
  var asideMenuFoot = $('.l-aside-menu__foot');

  function openAsideMenu() {
    $('body').addClass('js-menu-open');
    asideMenu.addClass('js-animate js-opening');
  }

  function closeAsideMenu() {
    $('body').removeClass('js-menu-open');
    asideMenu.removeClass('js-animate');
    setTimeout(function () {
      asideMenu.removeClass('js-opening');
    }, 150);
  }

  asideMenuBtn.on('pointerup', function (event) {
    event.preventDefault();
    openAsideMenu();
  });
  $('.l-aside-menu__close').on('pointerup', function (event) {
    event.preventDefault();
    closeAsideMenu();
  });
  $('.l-aside-menu__overlay').on('pointerup', function (event) {
    event.preventDefault();
    closeAsideMenu();
  });
  /**
   * запрещаем прокрутку страницы при прокрутке бокового-мобильного
   */

  $.preventScrolling($('.l-aside-menu__scroller'));
  /**
   * Клонирование верхнего-левого меню в боковое-мобильное
   */

  /*if ($.exists('.app-header__search')) {
  
      var newSearch = $('.app-header__search').clone();
  
      newSearch
          //.removeClass('app-header__catalog-list')
          .addClass('aside-nav-list__search')
          .appendTo(asideMenuScroller);
  
  
  }*/

  if ($.exists('.app-header__menu')) {
    var newMainMenu = $('.app-header__menu').clone();
    newMainMenu.removeClass('app-header__menu').addClass('aside-nav-list aside-nav-list__menu').appendTo(asideMenuScroller);
  }
  /*Добавление стрелочек для li*/


  $.each(asideMenuScroller.find('li'), function (index, element) {
    if ($(element).find('ul').length) {
      var triggerIcon = ['<svg xmlns="http://www.w3.org/2000/svg" width="14" height="9" viewBox="0 0 14 9" fill="none"><path d="M1 1L7 7L13 1" stroke="currentColor" stroke-width="2.5"></path></svg>'].join('');
      var subMenuTrigger = $('<div class="sub-menu-trigger">' + triggerIcon + '</div>');
      $(element).addClass('is-has-child');
      $(element).find(' > span').append(subMenuTrigger);
    }
  });
  $(document).on('click', '.aside-nav-list__menu .is-has-child', function (event) {
    event.preventDefault();
    $('.aside-nav-list__menu .is-has-child').removeClass('active').find('>ul').slideUp();
    $(this).stop().toggleClass('active').find('>ul').slideToggle();
  });
  var callbackPopup = $('[data-remodal-id=callback]').remodal();
  $('._js-callback').on('click', function (event) {
    event.preventDefault();
    callbackPopup.open();
  });
  ;

  (function () {
    if (!$('#map').length) {
      return;
    }

    ymaps.ready(init);
    var myMap, myPlacemark, myPlacemark2;

    function init() {
      myMap = new ymaps.Map("map", {
        center: [55.798022, 37.497660],
        zoom: 16
      });
      myPlacemark = new ymaps.Placemark([55.798022, 37.497660], {
        hintContent: 'улица Маршала Рыбалко,2'
      }, {
        iconLayout: 'default#image',
        iconImageHref: '../img/map-icon.png',
        iconImageSize: [30, 40]
      });
      myPlacemark2 = new ymaps.Placemark([55.868915, 37.583661], {
        hintContent: 'Алтуфьевское шоссе, д.41'
      }, {
        iconLayout: 'default#image',
        iconImageHref: '../img/map-icon.png',
        iconImageSize: [30, 40]
      });
      myMap.geoObjects.add(myPlacemark).add(myPlacemark2);
      $('.l-contacts__map-coordinat').on('click', function (event) {
        event.preventDefault();

        if (window.matchMedia('(max-width: 768px)').matches) {
          var mapOffset = $('#map').offset().top - $('.app-header__topline').outerHeight();
          $('html,body').animate({
            'scrollTop': mapOffset
          }, 700);
        }

        var coords = $(this).data('coordinates').split(",");
        var latitude = parseFloat(coords[0]);
        var longitude = parseFloat(coords[1]);
        myMap.panTo([latitude, longitude], {
          duration: 3000
        });
      });
    }
  })();

  $('.l-lot__slider').addClass('owl-carousel').owlCarousel({
    items: 1,
    autoplay: false,
    mouseDrag: false,
    touchDrag: true,
    nav: true,
    dots: false,
    navText: ['<svg xmlns="http://www.w3.org/2000/svg" width="14" height="26" viewBox="0 0 14 26" fill="none"><path d="M1.77036 25.3287C1.59671 25.5024 1.37966 25.5892 1.1409 25.5892C0.90214 25.5892 0.685085 25.5024 0.511442 25.3287C0.164155 24.9814 0.164155 24.4171 0.511442 24.0698L11.5812 13L0.511442 1.93027C0.164155 1.58298 0.164155 1.01864 0.511442 0.671354C0.858729 0.324067 1.42307 0.324067 1.77036 0.671354L13.4696 12.3706C13.8169 12.7179 13.8169 13.2822 13.4696 13.6295L1.77036 25.3287Z"/></svg>', '<svg xmlns="http://www.w3.org/2000/svg" width="14" height="26" viewBox="0 0 14 26" fill="none"><path d="M1.77036 25.3287C1.59671 25.5024 1.37966 25.5892 1.1409 25.5892C0.90214 25.5892 0.685085 25.5024 0.511442 25.3287C0.164155 24.9814 0.164155 24.4171 0.511442 24.0698L11.5812 13L0.511442 1.93027C0.164155 1.58298 0.164155 1.01864 0.511442 0.671354C0.858729 0.324067 1.42307 0.324067 1.77036 0.671354L13.4696 12.3706C13.8169 12.7179 13.8169 13.2822 13.4696 13.6295L1.77036 25.3287Z""/></svg>'],
    loop: false
  });
  $('.l-main-slider__items').addClass('owl-carousel').owlCarousel({
    items: 1,
    autoplay: false,
    loop: true,
    mouseDrag: false,
    touchDrag: true,
    animateOut: 'fadeOut',
    dots: true
  });
  var PAGE = $('html, body');
  var pageScroller = $('.page-scroller');
  var inMemoryClass = 'page-scroller--memorized';
  var isVisibleClass = 'page-scroller--visible';
  var enabledOffset = 60;
  var pageYOffset = 0;
  var inMemory = false;

  function resetPageScroller() {
    setTimeout(function () {
      if (window.pageYOffset > enabledOffset) {
        pageScroller.addClass(isVisibleClass);
      } else if (!pageScroller.hasClass(inMemoryClass)) {
        pageScroller.removeClass(isVisibleClass);
      }
    }, 150);

    if (!inMemory) {
      pageYOffset = 0;
      pageScroller.removeClass(inMemoryClass);
    }

    inMemory = false;
  }

  if (pageScroller.length > 0) {
    window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
      passive: true
    } : false);
    pageScroller.on('click', function (event) {
      event.preventDefault();
      window.removeEventListener('scroll', resetPageScroller);

      if (window.pageYOffset > 0 && pageYOffset === 0) {
        inMemory = true;
        pageYOffset = window.pageYOffset;
        pageScroller.addClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: 0
        }, 500, 'swing', function () {
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      } else {
        pageScroller.removeClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: pageYOffset
        }, 500, 'swing', function () {
          pageYOffset = 0;
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      }
    });
  }

  $('.l-partners__block').addClass('owl-carousel').owlCarousel({
    items: 6,
    autoplay: false,
    mouseDrag: false,
    touchDrag: true,
    margin: 25,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      600: {
        items: 3
      },
      768: {
        items: 4
      },
      1024: {
        items: 6
      }
    }
  });
  $('.l-popular__grid.slider').addClass('owl-carousel').owlCarousel({
    items: 4,
    autoplay: false,
    mouseDrag: false,
    touchDrag: true,
    nav: true,
    dots: false,
    navText: ['<svg xmlns="http://www.w3.org/2000/svg" width="14" height="26" viewBox="0 0 14 26" fill="none"><path d="M1.77036 25.3287C1.59671 25.5024 1.37966 25.5892 1.1409 25.5892C0.90214 25.5892 0.685085 25.5024 0.511442 25.3287C0.164155 24.9814 0.164155 24.4171 0.511442 24.0698L11.5812 13L0.511442 1.93027C0.164155 1.58298 0.164155 1.01864 0.511442 0.671354C0.858729 0.324067 1.42307 0.324067 1.77036 0.671354L13.4696 12.3706C13.8169 12.7179 13.8169 13.2822 13.4696 13.6295L1.77036 25.3287Z"/></svg>', '<svg xmlns="http://www.w3.org/2000/svg" width="14" height="26" viewBox="0 0 14 26" fill="none"><path d="M1.77036 25.3287C1.59671 25.5024 1.37966 25.5892 1.1409 25.5892C0.90214 25.5892 0.685085 25.5024 0.511442 25.3287C0.164155 24.9814 0.164155 24.4171 0.511442 24.0698L11.5812 13L0.511442 1.93027C0.164155 1.58298 0.164155 1.01864 0.511442 0.671354C0.858729 0.324067 1.42307 0.324067 1.77036 0.671354L13.4696 12.3706C13.8169 12.7179 13.8169 13.2822 13.4696 13.6295L1.77036 25.3287Z""/></svg>'],
    loop: true,
    margin: 30,
    responsive: {
      0: {
        items: 1
      },
      480: {
        items: 2
      },
      768: {
        items: 3
      },
      1024: {
        items: 4
      }
    }
  });
  $('.l-reviews__items').addClass('owl-carousel').owlCarousel({
    items: 1,
    autoplay: false,
    mouseDrag: false,
    touchDrag: true,
    nav: true,
    dots: false,
    navText: ['<svg xmlns="http://www.w3.org/2000/svg" width="14" height="26" viewBox="0 0 14 26" fill="none"><path d="M1.77036 25.3287C1.59671 25.5024 1.37966 25.5892 1.1409 25.5892C0.90214 25.5892 0.685085 25.5024 0.511442 25.3287C0.164155 24.9814 0.164155 24.4171 0.511442 24.0698L11.5812 13L0.511442 1.93027C0.164155 1.58298 0.164155 1.01864 0.511442 0.671354C0.858729 0.324067 1.42307 0.324067 1.77036 0.671354L13.4696 12.3706C13.8169 12.7179 13.8169 13.2822 13.4696 13.6295L1.77036 25.3287Z"/></svg>', '<svg xmlns="http://www.w3.org/2000/svg" width="14" height="26" viewBox="0 0 14 26" fill="none"><path d="M1.77036 25.3287C1.59671 25.5024 1.37966 25.5892 1.1409 25.5892C0.90214 25.5892 0.685085 25.5024 0.511442 25.3287C0.164155 24.9814 0.164155 24.4171 0.511442 24.0698L11.5812 13L0.511442 1.93027C0.164155 1.58298 0.164155 1.01864 0.511442 0.671354C0.858729 0.324067 1.42307 0.324067 1.77036 0.671354L13.4696 12.3706C13.8169 12.7179 13.8169 13.2822 13.4696 13.6295L1.77036 25.3287Z""/></svg>'],
    loop: true,
    animateOut: 'fadeOut'
  });
  ;

  (function ($) {
    $.fn.myTabs = function (options) {
      var settings = $.extend({
        title: '.l-tabs__title',
        body: '.l-tabs__body',
        responsive: false,
        responsiveWidth: 768,
        adaptiveOffset: 0,
        activeClass: 'active',
        afterInit: $.noop(),
        afterChange: $.noop()
      }, options);
      return $(this).each(function () {
        var titles = $(this).find(settings.title);
        var bodys = $(this).find(settings.body);
        var titlesParent = titles.parent();
        var bodysParent = bodys.parent();
        var flag = false;
        titles.each(function (ind, title) {
          $(title).attr('data-tab-index', ind);
          bodys.eq(ind).attr('data-tab-index', ind);
        });

        if (settings.responsive) {
          $(window).on('resize', function () {
            if (window.matchMedia("(max-width:" + settings.responsiveWidth + "px)").matches) {
              if (!flag) {
                titles.filter('.' + settings.activeClass).after(bodysParent);
                flag = true;
              }
            } else {
              if (flag) {
                titlesParent.after(bodysParent);
                flag = false;
              }
            }
          }).trigger('resize');
        }

        titles.on('click', function (event) {
          event.preventDefault();
          var $this = $(this);
          var indx = $this.data('tab-index');

          if ($this.hasClass(settings.activeClass)) {
            return;
          }

          titles.removeClass(settings.activeClass);
          $this.addClass(settings.activeClass);
          bodys.hide().removeClass(settings.activeClass).filter('[data-tab-index="' + indx + '"]').addClass(settings.activeClass).fadeIn();

          if (settings.responsive) {
            if (window.matchMedia("(max-width:" + settings.responsiveWidth + "px)").matches) {
              titles.filter('.' + settings.activeClass).after(bodysParent);
              var scrollBlock = titles.filter('.' + settings.activeClass).offset().top;
              var fixedTop = settings.adaptiveOffset; // $('html, body').animate({scrollTop: (scrollBlock - fixedTop)}, 700);
            }
          }

          if ($.isFunction(settings.afterChange)) {
            settings.afterChange();
          }
        });
        titles.eq(0).trigger('click');

        if ($.isFunction(settings.afterInit)) {
          settings.afterInit();
        }
      });
    };
  })(jQuery); // $('.l-tabs').myTabs({
  //     title: '.l-tabs__link',
  //     body: '.l-tabs__block',
  //     responsive: true
  // });


  $('.l-tabs-tech').myTabs({
    title: '.l-tabs-tech__link',
    body: '.l-tabs-tech__block',
    responsive: false
  });
  $('#responsiveTabs').responsiveTabs({
    startCollapsed: 'accordion',
    navigationContainer: '.l-tabs__links-wrap'
  });
  $(document).on('click', '.l-tabs-tech__link', function (event) {
    if (window.matchMedia("(max-width: 768px)").matches) {
      $('html, body').animate({
        scrollTop: $('.l-tabs-tech__block.active').offset().top - $('.app-header__topline').outerHeight()
      }, 700);
    }
  });
  var toggleMinHeight = $('.l-toggle-text-block__limiter').data('min-height');
  var togglemaxHeight = $('.l-toggle-text-block__all').outerHeight();
  var toggleHeight = toggleMinHeight;
  $('.l-toggle-text-block__limiter').css('height', toggleMinHeight);

  if (togglemaxHeight <= toggleMinHeight) {
    $('.l-toggle-text-block__more-link').detach();
    $('.l-toggle-text-block__limiter').css('height', 'auto');
  }

  $('.l-toggle-text-block__more-link a').on('click', function (event) {
    event.preventDefault(); // $('.content__limiter').animate({'height': height = height < togglemaxHeight ? togglemaxHeight : toggleMinHeight});

    $(this).toggleClass('active');

    if ($(this).hasClass('active')) {
      $('.l-toggle-text-block__limiter').animate({
        'height': togglemaxHeight
      });
    } else {
      $('.l-toggle-text-block__limiter').animate({
        'height': toggleMinHeight
      });
    }
  });
});